HOMEPAGE = "https://github.com/drgarcia1986/simple-settings"
SUMMARY = "A simple way to manage your project settings"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://setup.py;md5=cbe23847bfbb524ed035df09fe282367"

SRC_URI[md5sum] = "9c1b26024765db6fd21d46bd303cbd7c"
SRC_URI[sha256sum] = "6179c61bdc3daf5b7a5fd5dcc4da5cf70f92d65bd64662f75cdef5ae810f2b5c"


inherit pypi