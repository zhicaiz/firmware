#
# Setup a reception site testing system
#

SUMMARY = "Reception site tester setup"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://receptionsetup"
SRCREV = "${AUTOREV}"

INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME = "receptionsetup"

inherit update-rc.d

do_install_append() {
    # Install init script
    install -d ${D}${INIT_D_DIR}
    install -m 0755 ${WORKDIR}/receptionsetup ${D}${INIT_D_DIR}/receptionsetup


    echo ${PV} > ${D}${sysconfdir}/pwb-release
}

FILES_${PN} += "${INIT_D_DIR}/receptionsetup"
