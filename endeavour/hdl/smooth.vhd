library ieee;
use ieee.std_logic_1164.all;

----
-- Entity: smooth
-- Author: Karol Krizka <kkrizka@gmail.com>
--
-- Simple smoothing for over-sampled input.
--
-- The algorithm decides on the value of bit X by doing majority vote on the
-- input bits X-2,X-1,X,X+1,x+2.
--
-- The following decribes the behaviour for a simplified nearest neighbor check:
--
-- 110 -> 1
-- 011 -> 1
-- 111 -> 1
-- 101 -> 1
-- 001 -> 0
-- 100 -> 0
-- 000 -> 0
-- 010 -> 0
--
----
entity smooth is
  port (
    clock               : in  std_logic;
    reset               : in  std_logic;

    input               : in  std_logic;
    output              : out std_logic
    );
end entity smooth;

architecture behavioural of smooth is

  signal vote012 : std_logic;
  signal vote013 : std_logic;
  signal vote014 : std_logic;
  signal vote023 : std_logic;
  signal vote024 : std_logic;
  signal vote034 : std_logic;
  signal vote123 : std_logic;
  signal vote124 : std_logic;
  signal vote134 : std_logic;
  signal vote234 : std_logic;

  signal reg_data       : std_logic_vector(4 downto 0) := (others => '0');
begin
  process (clock)
  begin
    if rising_edge(clock) then
      if reset = '1' then
        reg_data        <= (others => '0');
      else
        reg_data        <= reg_data(3 downto 0) & input;
      end if;
    end if;
  end process;

  vote012 <= reg_data(0) and reg_data(1) and reg_data(2);
  vote013 <= reg_data(0) and reg_data(1) and reg_data(3);
  vote014 <= reg_data(0) and reg_data(1) and reg_data(4);
  vote023 <= reg_data(0) and reg_data(2) and reg_data(3);
  vote024 <= reg_data(0) and reg_data(2) and reg_data(4);
  vote034 <= reg_data(0) and reg_data(3) and reg_data(4);
  vote123 <= reg_data(1) and reg_data(2) and reg_data(3);
  vote124 <= reg_data(1) and reg_data(2) and reg_data(4);
  vote134 <= reg_data(1) and reg_data(3) and reg_data(4);
  vote234 <= reg_data(2) and reg_data(3) and reg_data(4);

  output <= vote012 or vote013 or vote014 or vote023 or vote024 or vote034 or vote123 or vote124 or vote134 or vote234;

end behavioural;

