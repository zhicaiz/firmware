v0.3.0 (unreleased)
- Update powertools to v1.6.1
- Use Hog to build firmware.
- Upgrade petalinux to 2020.3
