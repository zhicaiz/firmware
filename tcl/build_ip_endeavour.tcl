puts "################################################################################"
puts "## Generating Endeavour IP"
puts "################################################################################"

ipx::package_project -root_dir $globalSettings::user_ip_repo/endeavour -vendor lbl.gov -library pbv3 -taxonomy pbv3 -import_files

set_property supported_families {zynq Production} [ipx::current_core]
set_property name endeavour [ipx::current_core]
set_property display_name {Endeavour AXI Controller} [ipx::current_core]
set_property description {Endeavour protocol controlled via AXI bus} [ipx::current_core]
set_property vendor_display_name {Lawrence Berkeley National Laboratory} [ipx::current_core]

update_compile_order -fileset sources_1

ipx::update_source_project_archive -component [ipx::current_core]

ipx::save_core [ipx::current_core]
update_ip_catalog
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
ipx::unload_core [ipx::current_core]
