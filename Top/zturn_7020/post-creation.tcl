source ${repo_path}/bd/ActiveBoard.tcl

create_bd_design ActiveBoard
current_bd_design ActiveBoard

create_root_design {} {0} {5}

# Remove pull-ups on ULPI
set_property -dict [ list \
			 CONFIG.PCW_MIO_28_PULLUP {disabled} \
			 CONFIG.PCW_MIO_29_PULLUP {disabled} \
			 CONFIG.PCW_MIO_30_PULLUP {disabled} \
			 CONFIG.PCW_MIO_31_PULLUP {disabled} \
			 CONFIG.PCW_MIO_32_PULLUP {disabled} \
			 CONFIG.PCW_MIO_33_PULLUP {disabled} \
			 CONFIG.PCW_MIO_34_PULLUP {disabled} \
			 CONFIG.PCW_MIO_35_PULLUP {disabled} \
			 CONFIG.PCW_MIO_36_PULLUP {disabled} \
			 CONFIG.PCW_MIO_37_PULLUP {disabled} \
			 CONFIG.PCW_MIO_38_PULLUP {disabled} \
			 CONFIG.PCW_MIO_39_PULLUP {disabled} \
			] [get_bd_cells processing_system7_0]

# I2C0 EMIO connections
create_bd_intf_port -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 I2C0
connect_bd_intf_net [get_bd_intf_port I2C0] /processing_system7_0/IIC_0

# Create HDL wrapper
make_wrapper -files [get_files ActiveBoard.bd] -top -import
set_property top ActiveBoard_wrapper [current_fileset]
