#
# Hardware description

# Export
set xsa_file [file normalize "$work_path/$top_name.xsa"]

Msg Info "Exporting hardware configuration file to $xsa_file."
write_hw_platform -fixed -force -file $xsa_file

# Copy
set dst_xsa [file normalize "$dst_dir/$proj_name\-$describe.xsa"]

Msg Info "Copying XSA file $xsa_file into $dst_xsa."
file copy -force $xsa_file $dst_xsa
