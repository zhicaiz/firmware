# I2C
set_property PACKAGE_PIN D19 [get_ports iic_rtl0_scl_io];
set_property PACKAGE_PIN D20 [get_ports iic_rtl0_sda_io];

set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl0_scl_io]
set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl0_sda_io]

# SPI
set_property PACKAGE_PIN D18 [get_ports {spi_rtl0_ss_io[0]}];
set_property PACKAGE_PIN E17 [get_ports {spi_rtl0_sck_io}];
set_property PACKAGE_PIN B20 [get_ports spi_rtl0_io1_io];
set_property PACKAGE_PIN C20 [get_ports spi_rtl0_io0_io];

set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_sck_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_io0_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_io1_io]
set_property IOSTANDARD LVCMOS25 [get_ports {spi_rtl0_ss_io[0]}]

# AMAC
set_property PACKAGE_PIN E18 [get_ports {CMD_OUT_P_0}];
set_property PACKAGE_PIN E19 [get_ports {CMD_OUT_N_0}];
set_property PACKAGE_PIN B19 [get_ports {CMD_IN_P_0}];
set_property PACKAGE_PIN A20 [get_ports {CMD_IN_N_0}];

set_property IOSTANDARD LVDS_25 [get_ports CMD_IN_P_0]
set_property IOSTANDARD LVDS_25 [get_ports CMD_OUT_P_0]
set_property DIFF_TERM true [get_ports CMD_OUT_N_0]
